# Esto es un comentario. Python va a ignorarlo y no va a ejecutar nada ya que es solo un comentario

# Declaración de una variable de tipo número
# Python utiliza snake_case para los nombres de las variables
# int -> significa integer -> número entero
# La posibilidad de especificar los tipos de datos en Python se conoce como "Type Hints"

operand_a: int = 102
operand_b: float = 200.567

print(type(operand_a))
print(type(operand_b))

# Podemos utilizar operadores para realizar operaciones aritméticas y lógicas entre variables
# Los operadores aritméticos son los siguientes:

# Suma (+)
print(operand_a + operand_b)

# Resta (-)
print(operand_a - operand_b)

# Multiplicación
print(operand_a * operand_b)

# División ----------------- (Tenemos Tres Casos)
print(13 / 2)  # Esta división incluye decimales
print(13 // 2)  # Esta división es una división entera y no incluye puntos decimales
print(13 % 2)  # Esta división nos permite obtener el resultado del residuo de la división

result: float = 13 / 3

print(result)

# Cómo podemos entonces utilizar las variables y los operadores de manera útil?

base = 150
height = 50

area_of_the_triangle: float = (base * height) / 2

print(area_of_the_triangle)


